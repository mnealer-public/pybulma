# PyBulma Project Compile command

Creating a series of objects and manually generating templates is going to be too complex. We need to have
a system that will take in settings, compile items from a given directory and sub-directories, into a directory
stated in the settings.

We could build a system based on stating the structure in Yaml or similar like mkdocs, but that seems a bit long
winded. We would also need to state multiple parameters for each item. Also mkdocs just deals with files, while
we would possibly be dealing with multiple classes in each file.

First off do we use the directory structure of the web component library to state where the html files 
will be compiled. I don't think so. While that makes sense in terms of mkdocs, it doesn't really here, where
we could have a class that will generate multiple files, not just a single one.

So what if we pass a path parameter as a class variable. We could also pass a parameter to state if the class
should be compiled or not, as the class might be just a subclass to be used in other ones.

Ok, lets say we have a Compile Class, to which we pass a path variable. As a property, lets say it has a 
dictionary where the name of the file to be created is the key and the value is an object. That way we can search
for the Compile Classes and generate them. No instead of a class object as a value it should be the compiled html
that way a method or function can create multiple templates.

So if we have a bunch of Compile classes, then we just need to run the specific methods of those classes for
a complete compile to work. So do we need a central file or object. We want to have a central list of the classes
need to run, but do we want to run around importing them etc

