from base import Compile
from page import Page
from pydoc import locate


class Compile1(Compile):
    path = ""

    def build(self):
        pg1 = Page(attributes={"lang": "en"})
        self.add_component("page1", pg1)
        self.add_component("page2", pg1)


def main():
    my_classes = [
        "example_compile.compile1"
    ]
    for item in my_classes:
        cl = locate(item)
        obj = cl("/base/dir/")
        obj.compile()

