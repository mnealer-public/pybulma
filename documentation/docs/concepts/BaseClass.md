# PyBulma's Base Class Concept

All Pybulma classes are based on a single abstract class BaseComponent. This class is quite simple

## Required properties

The BaseComponent has only two required properties

### name
This is a string value representing the name assgined to the given class

### base_classes

base_classes is a list of strings value, detailing 0 or more classes that will be assigned to the html
element. So for the Button Component class, the base_classes is ["button"]

## Other properties

*sub_components* is a list that will hold subcomponents assigned to a class. For example, a Table, could
have subcomponents of TableHeader, TableBody and TableFooter. These in turn can have subcomponents of rows,
which will have TableCells as subcomponents

## Arguments

All arguments for the BaseComponent class are optional

* item_id : string value representing the ID of the html object. Is stored, pre-configured in self.id
* styles : Dictionary representing Style attributes. The values are configured into a style html argument and stored in self.styles
* attributes: Dictionary of html attributes. Values are configured into html, ready to be added
* classes: List of Strings, representing additional classes to be assigned to the html object. These are added to the classes stated in *base_classes*

## methods

### compile
This creates the html for the component. It triggers the compile method of components in the sub_components list

### sub_components
Lists the components held in sub_components

### add_component

Adds a component to the sub_component list. This should be a class based on the BaseComponent abstract class

### remove_component

Removes a component from the sub_component list, by list index number

### _pre_template (abstract method)
Creates the html code to be added before the sub_component html

### _post_template (abstact method)
creates the html tp be added after the sub_components html has been added
