# Why PyBulma

HTML is a pretty messy markup language with lots of different conventions. There are loads of different libraries
etc., out there trying to make up for this, however most of them are either templating languages or javascript
based. What I wanted was a nice library, something like Kotlin's Jetpack composer, that would allow me to code
the front end of my application using Python classes.

The problem here though is that HTML is a language, along with CSS to make almost anything on the frontend, whereas
jetpack creates based on specific components. To deal with this, I brought in the Bulma.io css library. This 
defined and formats specific frontend components for me, so I could then use the bulma classes to define 
components like columns, modals etc.

The second issue is that our html has to be dynamic. That is, the html is rendered based on the request from the
user. To do this we use a templating language like Jinja, so that it can take in a context object and render the
class. To deal with this, I build components that would generate jinja2 html rather than the completely rendered
html.

Thus, what I ended up with was a set of classes that I can use to create the html templates I need and compile
them into html. This is what PyBulma is.



