from .base import BaseComponent


class Page(BaseComponent):
    name = "html page"
    base_classes = []

    def __init__(self, load_static: bool = True, attributes: dict = None):
        self.load_static = load_static
        super().__init__(attributes=attributes)

    def _pre_template(self) -> str:
        if self.load_static:
            result = ["<!DOCTYPE html>", "<html"]
        else:
            result = ["<!DOCTYPE html>", "<html"]
        if self.attributes:
            result.append(self.attributes)
        result.append(">")
        return " ".join(result)

    def _post_template(self) -> str:
        return "</html>"


class Head(BaseComponent):
    name = "html head"
    base_classes = []

    def _pre_template(self) -> str:
        return "<head>"

    def _post_template(self) -> str:
        return "</head>"


class Meta(BaseComponent):
    name = "html meta"
    base_classes = []

    def _pre_template(self) -> str:
        result = ["<meta"]
        if self.attributes:
            result.append(self.attributes)
        result.append(">")
        return " ".join(result)

    def _post_template(self) -> str:
        return ""


class Title(BaseComponent):
    name = "html title"
    base_classes = []

    def __init__(self, title_text: str) -> None:
        self.title_text = title_text
        super().__init__()

    def _pre_template(self) -> str:
        return f"<title>{self.title_text}"

    def _post_template(self) -> str:
        return "</title>"


class Body(BaseComponent):
    name = "html body"
    base_classes = []

    def _pre_template(self) -> str:
        result = ["<body"]
        if self.id:
            result.append(self.id)
        if self.styles:
            result.append(self.styles)
        if self.attributes:
            result.append(self.attributes)
        result.append(">")
        return " ".join(result)

    def _post_template(self) -> str:
        return "</body>"
