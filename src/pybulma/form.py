from .base import BaseComponent


class Form(BaseComponent):
    name = "form"
    base_classes = []

    def __init__(self, item_id: str, action: str, method: str = "POST", styles: dict = None,
                 classes: list = None, attributes: str = None) -> None:
        super().__init__(item_id=item_id, styles=styles, attributes=attributes, classes=classes)
        self.method = method
        self.action = action

    def _pre_template(self) -> None:
        result = [f'<form', self.id]
        if self.method:
            result.append(f'method="{self.method}"')
        if self.action:
            result.append(f'action="{self.action}"')
        if self.attributes:
            result.append(self.attributes)
        if self.styles:
            result.append(self.styles)
        if self.classes:
            result.append(self.classes)
        result.append(">")
        return " ".join(result)

    def _post_template(self) -> None:
        return "</form>"


class Field(BaseComponent):
    name = "field container component"
    base_classes = ["field"]

    def _pre_template(self) -> str:
        result = ["<div", self.classes]
        if self.styles:
            result.append(self.styles)
        if self.id:
            result.append(self.id)
        if self.attributes:
            result.append(self.attributes)
        result.append(">")
        return " ".join(result)

    def _post_template(self) -> str:
        return "</div>"


class Control(Field):
    name = "Control Container Component"
    base_classes = ["control"]


class Label(BaseComponent):
    name = "input_label"
    base_classes = ["label"]

    def __init__(self, text: str, item_id: str = None, styles: dict = None,
                 attributes: dict = None, classes: list = None) -> None:
        super().__init__(item_id=item_id, styles=styles, attributes=attributes, classes=classes)
        self.text = text

    def _pre_template(self) -> str:
        result = ["<label", self.classes]
        if self.id:
            result.append(self.id)
        if self.styles:
            result.append(self.styles)
        if self.attributes:
            result.append(self.attributes)
        result.append(">")
        result.append(self.text)
        return " ".join(result)

    def _post_template(self) -> str:
        return "</label>"


class InputHelp(BaseComponent):
    name = "input_help"
    base_classes = ["help"]

    def __init__(self, text: str, item_id: str = None, classes: list = None,
                 styles: dict = None, attributes: dict = None) -> None:
        super().__init__(item_id=item_id, styles=styles, attributes=attributes, classes=classes)
        self.text = text

    def _pre_template(self) -> str:
        result = ["<p", self.classes]
        if self.id:
            result.append(self.id)
        if self.styles:
            result.append(self.styles)
        if self.attributes:
            result.append(self.attributes)
        result.append(">")
        result.append(self.text)
        return " ".join(result)

    def _post_template(self) -> str:
        return "</p>"


class Input(BaseComponent):
    name = "input_element"
    base_classes = ["input"]

    def __init__(self, item_id: str, name: str, input_type: str, value: str = None, placeholder: str = None,
                 styles: dict = None, classes: list = None, attributes: dict = None) -> None:
        super().__init__(item_id=item_id, styles=styles, attributes=attributes, classes=classes)
        self.name = f'name="{name}"'
        self.placeholder = placeholder
        self.input_type = f'type="{input_type}"'
        if value:
            self.value = f'value="{value}"'
        else:
            self.value = None

    def _pre_template(self) -> str:
        result = ["<input", self.id, self.classes, self.name]
        if self.value:
            result.append(self.value)
        if self.styles:
            result.append(self.styles)
        if self.attributes:
            result.append(self.attributes)
        if self.placeholder:
            result.append(self.placeholder)
        result.append(">")
        return " ".join(result)

    def _post_template(self) -> str:
        return " "


class TextArea(BaseComponent):
    name = "textarea"
    base_classes = ["textarea"]

    def __init__(self, name: str, value: str = None, item_id: str = None, placeholder: str = None,
                 classes: list = None, styles: dict = None, attributes: dict = None):
        super().__init__(item_id=item_id, styles=styles, attributes=attributes, classes=classes)
        self.name = f'name="{name}"'
        self.placeholder = placeholder
        self.value = value

    def _pre_template(self) -> str:
        result = ["<textarea", self.name, self.classes]
        if self.placeholder:
            result.append(self.placeholder)
        if self.attributes:
            result.append(self.attributes)
        if self.styles:
            result.append(self.styles)
        if self.id:
            result.append(self.id)
        result.append(">")
        if self.value:
            result.append(self.value)
        return " ".join(result)

    def _post_template(self) -> str:
        return "</textarea>"


class SelectContainer(BaseComponent):
    name = "select container"
    base_classes = "select"

    def _pre_template(self) -> str:
        result = ["<div", self.classes]
        if self.id:
            result.append(self.id)
        if self.styles:
            result.append(self.styles)
        if self.attributes:
            result.append(self.attributes)
        result.append(">")
        return " ".join(result)

    def _post_template(self) -> str:
        return "</div>"


class Select(BaseComponent):
    name = "select"
    base_classes = []

    def __init__(self, name: str, multiple: bool = False, item_id: str = None, styles: dict = None,
                 attributes: dict = None, classes: list = None) -> None:
        self.name = f'name="{name}"'
        self.multiple = multiple
        super().__init__(item_id=item_id, styles=styles, attributes=attributes, classes=classes)

    def _pre_template(self) -> str:
        result = ["<select", self.name]
        if self.multiple:
            result.append("multiple")
        if self.classes:
            result.append(self.classes)
        if self.id:
            result.append(self.id)
        if self.styles:
            result.append(self.styles)
        if self.attributes:
            result.append(self.attributes)
        result.append(">")
        return " ".join(result)

    def _post_template(self) -> str:
        return "</select>"


class Option(BaseComponent):
    name = "select option component"
    base_classes = []

    def __init__(self, text: str, value: str, selected: bool = False, item_id: str = None, styles: dict = None,
                 attributes: dict = None, classes: list = None) -> None:
        self.text = text
        self.value = f'value="{value}"'
        self.selected = selected
        super().__init__(item_id=item_id, styles=styles, attributes=attributes, classes=classes)

    def _pre_template(self) -> str:
        result = ["<option", self.value]
        if self.selected:
            result.append("selected")
        if self.id:
            result.append(self.id)
        if self.styles:
            result.append(self.styles)
        if self.attributes:
            result.append(self.attributes)
        if self.classes:
            result.append(self.classes)
        result.append(">")
        return " ".join(result)

    def _post_template(self) -> str:
        result = [self.text, "</option>"]
        return " ".join(result)


class CheckboxLabel(Label):
    name = "checkbox label"
    base_classes = ["checkbox"]


class CheckBox(BaseComponent):
    name = "checkbox"
    base_classes = []

    def __init__(self, name: str, value: str, checked: bool = False, item_id: str = None, styles: dict = None,
                 attributes: dict = None, classes: list = None) -> None:
        self.name = f'name="{name}"'
        self.value = f'value="{value}"'
        self.checked = checked
        super().__init__(item_id=item_id, styles=styles, attributes=attributes, classes=classes)

    def _pre_template(self) -> str:
        result = ['<input type="checkbox"', self.name, self.value]
        if self.checked:
            result.append("checked")
        if self.styles:
            result.append(self.styles)
        if self.attributes:
            result.append(self.attributes)
        if self.classes:
            result.append(self.classes)
        result.append(">")
        return " ".join(result)

    def _post_template(self) -> str:
        return ""


class RadioLabel(Label):
    name = "radio label"
    base_classes = ["radio"]


class Radio(CheckBox):
    name = "radio component"
    base_classes = []

    def _pre_template(self) -> str:
        result = ['<input type="radio"', self.name, self.value]
        if self.checked:
            result.append("checked")
        if self.styles:
            result.append(self.styles)
        if self.attributes:
            result.append(self.attributes)
        if self.classes:
            result.append(self.classes)
        result.append(">")
        return " ".join(result)


class FieldAddons(BaseComponent):
    name = "input_field_addons"
    base_classes = ["field", "has-addons"]

    def _pre_template(self) -> str:
        result = ["<div", self.classes]
        if self.id:
            result.append(self.id)
        if self.styles:
            result.append(self.styles)
        if self.attributes:
            result.append(self.attributes)
        result.append(">")
        return " ".join(result)

    def _post_template(self) -> str:
        return "</div>"


class FieldGrouped(BaseComponent):
    name = "input_field_grouped"
    base_classes = ["field", "is-grouped"]

    def _pre_template(self) -> str:
        result = ["<div", self.classes]
        if self.id:
            result.append(self.id)
        if self.styles:
            result.append(self.styles)
        if self.attributes:
            result.append(self.attributes)
        result.append(">")
        return " ".join(result)

    def _post_template(self) -> str:
        return "</div>"



