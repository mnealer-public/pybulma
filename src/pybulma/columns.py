from .base import BaseComponent


class Columns(BaseComponent):
    """Bulma Columns Container object"""
    name = "columns"
    base_classes = ["columns"]

    def _pre_template(self) -> str:
        result = ["<div", self.classes]
        if self.id:
            result.append(self.id)
        if self.styles:
            result.append(self.styles)
        if self.attributes:
            result.append(self.attributes)
        result.append(">")
        return " ".join(result)

    def _post_template(self) -> str:
        return "</div>"


class Column(BaseComponent):
    """Bulma Column object. Should be a child of a Columns Object"""
    name = "column"
    base_classes = ["column"]

    def _pre_template(self) -> str:
        result = ["<div", self.classes]
        if self.id:
            result.append(self.id)
        if self.styles:
            result.append(self.styles)
        if self.attributes:
            result.append(self.attributes)
        result.append(">")
        return " ".join(result)

    def _post_template(self) -> str:
        return "</div>"
